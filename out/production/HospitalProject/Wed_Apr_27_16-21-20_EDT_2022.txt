>Nurses
Suzy Rodrigo,101,golddust22
Timothy Smith,102,cuteweasel80
John Deer,103,ilovemytractor99
Emily Tack,104,wildwool57
<
>Doctors
~Jacob Barn,201,goodocean35
P*1,2,4,8,13
I*UPMC,AHN,United Health

~Peter McGriffin,202,emptypail23
P*3,5,6
I*UPMC,United Health

~Patricia Murphy,203,kindbutton55
P*7,9,10
I*UPMC,United Health,Anthem Inc

~Elizabeth Blake,204,blacksteam79
P*11,12
I*UPMC
<
>Patients
~Kirstie Mercer,1,United Health,07-22-1989,201,101,F,false
Il*Stomach Ache,Allergies
M*Pepto-Bismal,Claritin
V*@3,6,2022,9,0,false
@3,20,2022,9,0,true

~Donald Barry,2,UPMC,01-12-1990,201,102,M,false
Il*Muscular Injury,Headache
M*Naproxen,Ibuprofen
V*@10,4,2004,6,30,false
@10,11,2004,20,0,true
@6,21,2015,8,0,false
@7,1,2015,10,30,true

~Marie Kelly,3,UPMC,11-03-2011,202,101,F,false
Il*Broken Bone
M*Plastic Cast,Pain Relief
V*@2,13,2020,19,21,false
@2,27,2020,11,0,true
@3,1,2020,11,0,false

~Devin Boomer,4,AHN,01-01-2001,201,104,M,false
Il*Bronchitis
M*Nyal
V*@9,28,2009,21,33,false
@10,5,2009,10,0,false
@10,10,2009,9,0,true

~Harvey Spectre,5,United Health,09-10-2017,202,104,M,false
Il*Influenza,Cold
M*Mucinex,Tylenol
V*@4,5,2022,15,28,false

~Carl Spencer,6,United Health,11-15-2010,202,101,M,false
Il*Cold
M*Mucinex
V*@10,5,2019,19,21,false
@10,8,2019,7,0,true

~Patricia Holmes,7,Anthem Inc,08-20-1986,203,103,F,false
Il*Dementia
M*Donepezil Aricept
V*@3,30,2022,6,0,false

~Emily Trippe,8,AHN,12-25-2000,201,103,F,false
Il*Muscular Injury
M*Naproxen
V*@1,20,2014,9,45,false
@1,29,2014,9,30,false
@5,21,2014,9,0,true

~Charles Brickle,9,United Health,07-2-2003,203,101,M,false
Il*Muscular Injury,Anorexia,Influenza,Broken Bone,Cold
M*Naproxen,Mucinex,Tylenol,Plastic Cast,Pain Relief,Ibuprofen
V*@9,13,2011,8,0,false
@10,13,2011,8,0,false
@10,20,2011,8,0,true
@5,7,2013,21,34,false
@5,8,2013,9,0,false
@5,22,2013,9,0,true
@8,1,2018,18,4,false
@8,15,2018,7,0,true
@4,6,2022,15,56,false

~Marcy Thomas,10,UPMC,02-02-2002,203,102,F,false
Il*Cold
M*Mucinex,Tylenol
V*@4,4,2016,13,30,false
@4,11,2016,14,0,true

~Michael Johnson,11,UPMC,07-19-1970,204,101,M,false
Il*Influenza,Bronchitis
M*Mucinex,Tylenol,Nyal
V*@10,1,1991,6,30,false
@10,15,1991,6,30,false
@10,29,1991,6,30,true
@1,9,2000,8,0,false
@1,23,2000,8,0,true

~Annabell Marshallson,12,UPMC,03-20-2012,204,104,F,false
Il*Allergies,Cold
M*Claritin,Mucinex,Tylenol
V*@1,20,2020,9,0,false
@2,3,2020,9,0,true
@11,22,2021,13,0,false
@12,0,2021,9,0,false
@12,14,2021,9,0,true

~Johnny Stones,13,UPMC,11-22-1998,201,103,M,false
Il*Broken Nose,Severe Headache
M*Ibuprofen
V*
<