/**
 * Doctor.java:
 * This class models Doctor Objects. Child of Employee.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Models.People;

import java.util.ArrayList;


public class Doctor extends Employee
{
    private ArrayList<Patient> patientList = new ArrayList<>();
    private ArrayList<String> acceptedInsuranceList, patientIDList;

    //primary constructor for reading in from file
    public Doctor(String empName, int empID, String empPass, ArrayList<String> pIDL, ArrayList<String> aIL)
    {
        super(empName, empID, empPass);
        patientIDList = pIDL;
        acceptedInsuranceList = aIL;
    }

    public ArrayList<String> getPatientIDList() {
        return patientIDList;
    }

    public ArrayList<Patient> getPatientList() { return patientList; }

    public ArrayList<String> getAcceptedInsuranceList() {
        return acceptedInsuranceList;
    }

}
