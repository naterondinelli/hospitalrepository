/**
 * Patient.java:
 * Model for Patient Objects. Holds all critical info for patients,
 * including arraylists of visitations, illnesses, adn medications
 *
 * @authors Nate Rondinelli, Devin Schiffer
 *
 */


package Models.People;

import Models.Visitation;

import java.util.ArrayList;

public class Patient
{
    //Vars
    private String name, insurancePlan, dateOfBirth;
    private int ID;
    private Doctor doctor;
    private Nurse nurse;
    private char gender;
    private boolean isFollowedUpWith;
    private ArrayList<String> illnessList, medicationList;
    private ArrayList<Visitation> visitationList;

    //From file
    public Patient(String na,int id, String ip, String dob, Doctor d, Nurse nu, char g, boolean ifuw, ArrayList<String> il,
                   ArrayList<String> ml, ArrayList<Visitation> vl)
    {
        name = na;
        ID = id;
        insurancePlan = ip;
        dateOfBirth = dob;
        doctor = d;
        nurse = nu;
        gender = g;
        isFollowedUpWith = ifuw;
        illnessList = il;
        medicationList = ml;
        visitationList = vl;
    }

    //From user
    public Patient(String na, String ip, String dob, Doctor d, Nurse nu, char g, ArrayList<String> il,
                   ArrayList<String> ml)
    {
        name = na;
        insurancePlan = ip;
        dateOfBirth = dob;
        doctor = d;
        nurse = nu;
        gender = g;
        isFollowedUpWith = false;
        illnessList = il;
        medicationList = ml;
        visitationList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getInsurancePlan() {
        return insurancePlan;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public int getID() {
        return ID;
    }

    public void setID(int id)
    {
        this.ID = id;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public Nurse getNurse() {
        return nurse;
    }

    public char getGender() {
        return gender;
    }

    public Boolean getIsFollowedUpWith() {
        return isFollowedUpWith;
    }

    public ArrayList<String> getIllnessList() {
        return illnessList;
    }

    public ArrayList<String> getMedicationList() {
        return medicationList;
    }

    public ArrayList<Visitation> getVisitationList() {
        return visitationList;
    }

    /**
     * This method prints future appointments, to be used in the hospital class when displaying future appointments
     * @param month current month
     * @param day current day
     * @param year current year
     */
    public void printVisitationList(int month, int day, int year)
    {
        int i = 1;
        for (Visitation v : visitationList)
        {
            if(v.getYear() > year)
            {
                System.out.println("\tAppointment Index: " + i +";    "+ v);

            }
            else if(v.getYear() == year && v.getMonth() > month)
            {
                System.out.println("\tAppointment Index: " + i +";    "+ v);
            }
            else if(v.getYear() == year && v.getMonth() == month && v.getDay() > day)
            {
                System.out.println("\tAppointment Index: " + i +";    "+ v);
            }
            i++;
        }
    }
}
