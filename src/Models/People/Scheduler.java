/**
 * Scheduler.java:
 * This interface is implemented by NurseLogic and DoctorLogic.
 * allows a relationship to show who acts as a scheduler.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */

package Models.People;

public interface Scheduler
{
     void scheduleVisitation(Patient p);

     void cancelVisitation(Patient p, int index);
}