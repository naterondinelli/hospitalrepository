/**
 * Nurse.java:
 * Model for Nurse Objects. Child of Employee
 *
 * @authors Nate Rondinelli, Devin Schiffer
 *
 */

package Models.People;


public class Nurse extends Employee
{
    public Nurse(String empName, int empID, String empPass)
    {
        super(empName, empID, empPass);
    }
}

