/**
 * Employee.java:
 * Model for Employee Objects. Parent of Nurse and Doctor
 *
 * @authors Nate Rondinelli, Devin Schiffer
 *
 */
package Models.People;

public class Employee
{
    //instance variables
    private String name;
    private int ID;
    private String password;

    public Employee(String empName, int empID, String empPass)
    {
        name = empName;
        ID = empID;
        password = empPass;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

}

