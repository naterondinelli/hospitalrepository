/**
 * DataBase.java:
 * Models a "database" to hold arraylists of the people models to be used in the system
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */


package Models;

import Models.People.Doctor;
import Models.People.Nurse;
import Models.People.Patient;

import java.util.ArrayList;

public class DataBase
{
    protected ArrayList<Nurse> nurses = new ArrayList<>();
    protected ArrayList<Doctor> doctors = new ArrayList<>();
    protected ArrayList<Patient> patients = new ArrayList<>();

    public ArrayList<Doctor> getDoctors() {
        return doctors;
    }

    public ArrayList<Nurse> getNurses() {
        return nurses;
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }
}
