/**
 * Visitation.java:
 * Model for Visitation Objects. Held by patients
 *
 * @authors Nate Rondinelli, Devin Schiffer
 *
 */


package Models;

public class Visitation
{
    //Vars
    private int month, day, year, hours, minutes;
    private boolean checkUp;

    //from file
    public Visitation (int mo, int d, int y, int h, int mi, boolean checkup)
    {
        month = mo;
        day = d;
        year = y;
        hours = h;
        minutes = mi;
        checkUp = checkup;
    }

    public int getMonth()
    {
        return month;
    }

    public int getDay()
    {
        return day;
    }

    public int getYear()
    {
        return year;
    }

    public int getHours()
    {
        return hours;
    }

    public int getMinutes()
    {
        return minutes;
    }

    public boolean isCheckUp() {
        return checkUp;
    }

    /**
     * returns the visitation as a nicely formatted string
     * @return String of visitation
     */
    @Override
    public String toString()
    {
        String s = month + "/" + day + "/" + year + " @ " + hours + ':' + minutes;
        if(minutes == 0)
        {
            s+="0";
        }

        if(checkUp)
        {
            s+= "\t Check-up";
        }
        return s;
    }
}

