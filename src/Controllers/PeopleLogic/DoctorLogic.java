/**
 * DoctorLogic.java:
 * This class abstracts the logic used by doctors to add patients to their patientList, get info
 * about any of their patients, follow up with patients, and schedule/cancel appointments.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Controllers.PeopleLogic;

import Controllers.Exceptions.InvalidInsuranceException;
import Models.People.*;
import Models.Visitation;
import java.util.*;

public class DoctorLogic implements Scheduler
{
    /**
     * addPatientToList:
     * This method adds a patient object to a doctor's patientList. It checks to make sure the
     * patient insurance matches one of the doctor's accepted insurances.
     *
     * @param p Patient object
     * @param d Doctor object
     * @throws InvalidInsuranceException Exception for insurance mismatch
     */
    public void addPatientToList(Patient p, Doctor d) throws InvalidInsuranceException
    {
        boolean valid = false;
        for (String s: d.getAcceptedInsuranceList())
        {
            if(s.equalsIgnoreCase(p.getInsurancePlan()))
            {
                valid = true;
                //System.out.println("##valid Insurance for pat: " + p.getName());
                break;
            }
        }

        if (valid)
        {
            d.getPatientList().add(p);
            //System.out.println("## added pat: " + p.getName() + " to dr's list");
        }
        else
        {
            throw new InvalidInsuranceException();
        }
    }



    /**
     * cancelVisitation:
     * This method removes an appointment from a patients visitationList and has the doctor
     * schedule a new appointment for the patient It overrides the method from the Scheduler.java interface.
     *
     * @param p Patient object
     */
    @Override
    public void cancelVisitation(Patient p, int index)
    {
        p.getVisitationList().remove(p.getVisitationList().get(index));
        System.out.println("Appointment cancelled. Please schedule a new one: \n");

        scheduleVisitation(p);
    }

    /**
     * scheduleVisitation:
     * This method creates an appointment and validates the user inputs. It overrides the method
     * from the Scheduler.java interface.
     *
     * @param p Patient object
     */
    @Override
    public void scheduleVisitation(Patient p)
    {
        //local vars
        Scanner scan = new Scanner(System.in);
        int mo=0;
        int d=0;
        int y=0;
        int h=0;
        int mi=0;

        //ask for user inputs
        System.out.println("Please enter the patient's new illness (\"None\" if no new illness): ");
        String i = scan.nextLine();
        System.out.println("Enter the patient's new medication (\"None\" if no new medication):");
        String m = scan.nextLine();

        //validate the month
        System.out.println("Enter the month (1-12): ");
        boolean moValid = false;
        while(!moValid)
        {
            mo = scan.nextInt();
            if(mo >= 1 && mo <= 12)
            {
                moValid = true;
            }
            else
                System.out.println("Invalid. Enter the month (1-12): ");
        }

        //validate the day
        System.out.println("Enter the day (1-31): ");
        boolean dValid = false;
        while(!dValid)
        {
            d = scan.nextInt();
            if(d >= 1 && d <= 31)
            {
                dValid = true;
            }
            else
                System.out.println("Invalid. Enter the day (1-31): ");
        }

        //validate the year
        System.out.println("Enter the year (2022 or after)");
        boolean yValid = false;
        while(!yValid)
        {
            y = scan.nextInt();
            if(y >= 2022)
            {
                yValid = true;
            }
            else
                System.out.println("Invalid. Enter the year (2022 or after): ");
        }

        //validate the hour
        System.out.println("Enter the hour (0-23):");
        boolean hValid = false;
        while(!hValid)
        {
            h = scan.nextInt();
            if(h >= 1 && h <= 12)
            {
                hValid = true;
            }
            else
                System.out.println("Invalid. Enter the hour (0-23): ");
        }

        //validate the minute
        System.out.println("Enter the minute (0-59):");
        boolean miValid = false;
        while(!miValid)
        {
            mi = scan.nextInt();
            if(mi >= 0 && mi <= 59)
            {
                miValid = true;
            }
            else
                System.out.println("Invalid. Enter the minute (0-59): ");
        }

        //create new visitation and add new illness/medication to the illness/medication lists
        //Note: doctors automatically schedule non check-up visitations
        Visitation v = new Visitation(mo, d, y, h, mi,false);
        if(!i.equalsIgnoreCase("None"))
            p.getIllnessList().add(i);
        if(!m.equalsIgnoreCase("None"))
            p.getMedicationList().add(m);
        p.getVisitationList().add(v);

        System.out.println("Visitation for " + p.getName() + " scheduled!\n");
    }
}