/**
 * NurseLogic.java:
 * This class abstracts the logic used by nurses to get info about any of their patients,
 * follow up with patients, and schedule/cancel appointments.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Controllers.PeopleLogic;

import Models.People.*;
import Models.Visitation;
import java.util.*;

public class NurseLogic implements Scheduler
{


    /**
     * scheduleVisitation:
     * This method creates an appointment and validates the user inputs. It overrides the method
     * from the Scheduler.java interface.
     *
     * @param p Patient object
     */
    @Override
    public void scheduleVisitation(Patient p)
    {
        //locals vars
        Scanner scan = new Scanner(System.in);
        int mo=0;
        int d=0;
        int y=0;
        int h=0;
        int mi=0;

        //validate the month
        System.out.println("Enter the month (1-12): ");
        boolean moValid = false;
        while(!moValid)
        {
            mo = scan.nextInt();
            if(mo >= 1 && mo <= 12)
            {
                moValid = true;
            }
            else
                System.out.println("Invalid. Enter the month (1-12): ");
        }

        //validate the day
        System.out.println("Enter the day (1-31): ");
        boolean dValid = false;
        while(!dValid)
        {
            d = scan.nextInt();
            if(d >= 1 && d <= 31)
            {
                dValid = true;
            }
            else
                System.out.println("Invalid. Enter the day (1-31): ");
        }

        //validate the year
        System.out.println("Enter the year (2022 or after)");
        boolean yValid = false;
        while(!yValid)
        {
            y = scan.nextInt();
            if(y >= 2022)
            {
                yValid = true;
            }
            else
                System.out.println("Invalid. Enter the year (2022 or after): ");
        }

        //validate the hour
        System.out.println("Enter the hour (0-23):");
        boolean hValid = false;
        while(!hValid)
        {
            h = scan.nextInt();
            if(h >= 0 && h <= 23)
            {
                hValid = true;
            }
            else
                System.out.println("Invalid. Enter the hour (0-23): ");
        }

        //validate the minute
        System.out.println("Enter the minute (0-59):");
        boolean miValid = false;
        while(!miValid)
        {
            mi = scan.nextInt();
            if(mi >= 0 && mi <= 59)
            {
                miValid = true;
            }
            else
                System.out.println("Invalid. Enter the minute (0-59): ");
        }

        //create new visitation
        //Note: nurses automatically schedule check-up visitations
        Visitation v = new Visitation(mo, d, y, h, mi, true);
        p.getVisitationList().add(v);
        System.out.println("Visitation for " + p.getName() + " scheduled!\n");
    }

    /**
     * cancelVisitation:
     * This method removes an appointment from a patients visitationList and sets a patient's followUp
     * status to false. It overrides the method from the Scheduler.java interface.
     *
     * @param p Patient object
     */
    @Override
    public void cancelVisitation(Patient p, int index)
    {
        p.getVisitationList().remove(p.getVisitationList().get(index));
    }

}