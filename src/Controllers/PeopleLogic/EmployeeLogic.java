/**
 * EmployeeLogic.java:
 * This class abstracts the logic used by employees to register new patients.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Controllers.PeopleLogic;

import Controllers.DataBaseLogic.SearchLogic;
import Controllers.Exceptions.DoctorNotFoundException;
import Controllers.Exceptions.NurseNotFoundException;
import Models.*;
import Models.People.*;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;


public class EmployeeLogic
{
    //instance variable(s)
    private DataBase db;

    /**
     * Constructor:
     * takes in a DataBase object and instantiates EmployeeLogic to be used in Main.
     *
     * @param db DataBase object
     */
    public EmployeeLogic(DataBase db)
    {
        this.db = db;
    }


    /**
     * getInfo:
     * This method prints the personal patient information doctors have access to.
     *
     * @param p Patient object
     */
    public void getInfo(Patient p)
    {
        System.out.println("Patient: " + p.getName());
        System.out.println("Gender: " + p.getGender());
        System.out.println("Date of birth: " + p.getDateOfBirth()); //Change dob format later
        System.out.println("Insurance: " + p.getInsurancePlan());
        System.out.println("Doctor: " + p.getDoctor().getName());
        System.out.println("Nurse: " + p.getNurse().getName());

        System.out.println("Illnesses: ");
        for(String s: p.getIllnessList())
        {
            System.out.println("\t" + s);
        }
        System.out.println("Medications: ");
        for(String s: p.getMedicationList())
        {
            System.out.println("\t" + s);
        }
        System.out.println("Visitation History: ");
        for(Visitation v: p.getVisitationList())
        {
            System.out.println("\t" + v.toString());
        }
    }


    /**
     * registerPatient:
     * This method takes in an Employee object (or child of Employee) and creates a new patient object.
     * User inputs are validated to ensure the patient is assigned an active doctor/nurse.
     *
     * @param e Employee object (or child of Employee)
     * @return New Patient
     */
    public Patient registerPatient(Employee e)
    {
        //local vars
        Scanner scan = new Scanner(System.in);
        Doctor d = null;
        Nurse nu = null;
        char g = ' ';

        //ask for patient name, insurance, and birth date
        System.out.println("Enter patient's name:");
        String na = scan.nextLine();
        System.out.println("Enter patient's insurance plan:");
        String ip = scan.nextLine();
        System.out.println("Enter patient's date of birth (mm-dd-yyyy):");
        String dob = scan.next();

        //ask for doctor ID and validate input
        System.out.println("Enter patient's doctor ID:");
        boolean docFound = false;
        while(!docFound)
        {
          int docID = scan.nextInt();
          try
           {
               d = SearchLogic.findDoctor(db,docID);
               docFound = true;
           }catch(DoctorNotFoundException DNFE)
           {
               System.out.println(DNFE.getMessage());
               System.out.println("Try again. Enter patient's doctor ID: ");
           }
        }

        //ask for nurse ID and validate input
        System.out.println("Enter patient's nurse ID:");
        boolean nurseFound = false;
        while(!nurseFound)
        {
          int nurseID = scan.nextInt();
          try
           {
               nu = SearchLogic.findNurse(db,nurseID);
               nurseFound = true;
           }catch(NurseNotFoundException NNFE)
           {
               System.out.println(NNFE.getMessage());
               System.out.println("Try again. Enter patient's nurse ID: ");
           }
       }
        if (scan.hasNextLine())
            scan.nextLine();

        //ask for patient gender and validate input
        System.out.println("Enter patient's gender (M or F):");
        boolean genderValid = false;
        while(!genderValid)
        {
            String tmpG = scan.nextLine();
            if(tmpG.equalsIgnoreCase("M") || tmpG.equalsIgnoreCase("F"))
            {
                genderValid = true;
                g = tmpG.charAt(0);
            }
            else
                System.out.println("Try again. Enter patient's gender (M or F):");
        }

        //ask for illnesses and add to illnesses list
        ArrayList<String> il = new ArrayList<>();
        String illness = " ";
        boolean illCont = true;
        while(illCont)
        {
            System.out.println("Please enter a patient illness one by one (\"stop\" to end):");
            illness = scan.nextLine();

            if(illness.equalsIgnoreCase("stop"))
                illCont = false;
            else
            {
                il.add(illness);
            }
        }

        //ask for medications and add to medications list
        ArrayList<String> ml = new ArrayList<>();
        String medication = " ";
        boolean medCont = true;
        while(medCont)
        {
            System.out.println("Please enter a patient medication one by one (\"stop\" to end):");
            medication = scan.nextLine();

            if(medication.equalsIgnoreCase("stop"))
                medCont = false;
            else
            {
                ml.add(medication);
            }
        }

        //create and return new patient object

        Patient p = new Patient(na, ip, dob, d, nu, g, il, ml);
        int lastPatID = db.getPatients().get(db.getPatients().size() - 1).getID();
        p.setID(lastPatID + 1);
        createInitialVisit(p);
        return p;
    }

    /**isValidEmployeeLogin
     * This method takes in an ID and password and traverses the doctor/nurse lists to see
     * if the id and password match an employee.
     *
     * @param ID int employee id number
     * @param pw string employee password
     * @return boolean value
     */
    public boolean isValidEmployeeLogin(int ID, String pw)
    {
        //check doctor match
        if(ID >= 200 && ID < 300)
        {
            for (Doctor doctor: db.getDoctors())
            {
                if(ID == doctor.getID() && pw.equals(doctor.getPassword()))
                    return true;
            }
        }
        //check nurse match
        else if (ID >= 100 && ID < 200)
        {
            for (Nurse nurse: db.getNurses())
            {
                if(ID == nurse.getID() && pw.equals(nurse.getPassword()))
                    return true;
            }
        }
        //no match
        System.out.println("ID and Password does not match any employee, try again.\n");
        return false;
        //temp
    }

    /**
     * createInitialVisit:
     * creates the first visit when the patient is registered. Uses Java.Time.LocalDateTime
     *
     * @param p patient
     */
    public void createInitialVisit(Patient p)
    {
        Visitation v = new Visitation(LocalDateTime.now().getMonthValue(), LocalDateTime.now().getDayOfMonth(),
                LocalDateTime.now().getYear(), LocalDateTime.now().getHour(), LocalDateTime.now().getMinute(), false);

        p.getVisitationList().add(v);
    }

/*
    public static void main(String[] args) {
        ArrayList<String> pIDL = new ArrayList<>();
        ArrayList<String> aIL = new ArrayList<>();
        Doctor d = new Doctor("Steven",210,"redflowers16",pIDL,aIL);
        Nurse n = new Nurse("Johnathan",115,"bluerock7");

        ArrayList<String> il = new ArrayList<>();
        ArrayList<String> ml = new ArrayList<>();
        ArrayList<Visitation> vl = new ArrayList<>();

        Patient p = new Patient("john doe",23,"UPMC","12-24-2021",d,n,'M',false,il,ml,vl);

        createInitialVisit(p);

    }
    */
}

