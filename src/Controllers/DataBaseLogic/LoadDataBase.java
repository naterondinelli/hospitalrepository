/**
 * LoadDataBase.java:
 * This class reads in data from a file to load the previous state of the hospital. Users must provide
 * the file location.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Controllers.DataBaseLogic;

import Controllers.Exceptions.DoctorNotFoundException;
import Controllers.Exceptions.InvalidInsuranceException;
import Controllers.Exceptions.NurseNotFoundException;
import Controllers.Exceptions.PatientNotFoundException;
import Controllers.PeopleLogic.DoctorLogic;
import Models.DataBase;
import Models.People.*;
import Models.Visitation;

import java.io.*;
import java.rmi.ServerError;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class LoadDataBase
{
    //instance vars
    private Scanner inFile;
    private DataBase db;
    private DoctorLogic dl = new DoctorLogic();

    /**
     * Constructor:
     * takes in a DataBase object and instantiates LoadDataBase to be used in Main.
     *
     * @param db DataBase object
     */
    public LoadDataBase(DataBase db)
    {
        this.db = db;
    }

    /**
     * loadData:
     * This method opens a file connection and traverses the contents of the file to generate the
     * hospital state. It populates all the data held within the database. Ex: lists of doctors,
     * nurses, patients, and visitations.
     *
     * @param fIn File object
     */
    public boolean loadData(File fIn)
    {
        //create file connection
        try
        {
            inFile = new Scanner(fIn);
        } catch (FileNotFoundException FNFE)
        {
            System.err.println("File not found! Please try again!");
            return false;

        }

        //Main loop
        while (inFile.hasNextLine())
        {
            String line = inFile.nextLine();

            //Nurses
            if (line.startsWith(">Nurses"))
            {
                line = inFile.nextLine();
               while (inFile.hasNextLine() && !line.startsWith("<"))
               {
                   ArrayList<String> tmpNurse = new ArrayList<>(Arrays.asList(line.split(",")));
                   int i = Integer.parseInt(tmpNurse.get(1));
                   Nurse n = new Nurse(tmpNurse.get(0),i,tmpNurse.get(2));
                   db.getNurses().add(n);
                   //System.out.println("nurse " + n.getName() + " added");
                   line = inFile.nextLine();
               }
            }

            //Doctors part1
            line = inFile.nextLine();
            if (line.startsWith(">Doctors"))
            {
                line = inFile.nextLine();
                while (inFile.hasNextLine() && !line.startsWith("<"))
                {
                    if(line.equals(""))
                    {
                        line = inFile.nextLine();
                    }
                    if (line.startsWith("~"))
                    {
                        line = line.substring(1);
                        ArrayList<String> tmpDRMain = new ArrayList<>(Arrays.asList(line.split(",")));
                        int i = Integer.parseInt(tmpDRMain.get(1));
                        line = inFile.nextLine().substring(2);
                        ArrayList<String> tmpDRPats = new ArrayList<>(Arrays.asList(line.split(",")));
                        line = inFile.nextLine().substring(2);
                        ArrayList<String> tmpDRIL = new ArrayList<>(Arrays.asList(line.split(",")));
                        Doctor dr = new Doctor(tmpDRMain.get(0),i,tmpDRMain.get(2),tmpDRPats, tmpDRIL);
                        db.getDoctors().add(dr);
                        //System.out.println("doctor " + dr.getName() + " added");

                        line = inFile.nextLine();
                    }
                }
            }
            //Patients
            line = inFile.nextLine();
            if (line.startsWith(">Patients"))
            {
                line = inFile.nextLine();
                while (inFile.hasNextLine() && !line.startsWith("<"))
                {
                    if(line.equals(""))
                    {
                        line = inFile.nextLine();
                    }
                    if (line.startsWith("~"))
                    {
                        line = line.substring(1);
                        ArrayList<String> tmpPatMain = new ArrayList<>(Arrays.asList(line.split(",")));
                        int patID = Integer.parseInt(tmpPatMain.get(1));
                        int patDRID = Integer.parseInt(tmpPatMain.get(4));
                        int patNurseID = Integer.parseInt(tmpPatMain.get(5));
                        boolean followUp = tmpPatMain.get(6).equals("true");

                        line = inFile.nextLine().substring(3);
                        ArrayList<String> tmpPatIllnesses = new ArrayList<>(Arrays.asList(line.split(",")));
                        line = inFile.nextLine().substring(2);
                        ArrayList<String> tmpPatMeds = new ArrayList<>(Arrays.asList(line.split(",")));


                        Doctor patDR = null;
                        Nurse patNurse = null;
                        try
                        {
                            patDR = SearchLogic.findDoctor(db,patDRID);
                            patNurse = SearchLogic.findNurse(db,patNurseID);
                        } catch (DoctorNotFoundException DNFE)
                        {
                            System.out.println(DNFE.getMessage());
                        } catch (NurseNotFoundException NNFE)
                        {
                            System.out.println(NNFE.getMessage());
                        }


                        //Visitations
                        ArrayList<Visitation> tmpPatVisits = new ArrayList<>();
                        line = inFile.nextLine().substring(2);
                        while(line.startsWith("@"))
                        {
                            line = line.substring(1);
                            ArrayList<String> tmpVisitaion = new ArrayList<>(Arrays.asList(line.split(",")));
                            boolean checkup = tmpVisitaion.get(5).equals("true");
                            Visitation v = new Visitation(Integer.parseInt(tmpVisitaion.get(0)) ,
                                    Integer.parseInt(tmpVisitaion.get(1)),Integer.parseInt(tmpVisitaion.get(2)),
                                    Integer.parseInt(tmpVisitaion.get(3)), Integer.parseInt(tmpVisitaion.get(4)),
                                    checkup);
                            tmpPatVisits.add(v);
                            line = inFile.nextLine();
                        }


                        if (patDR != null && patNurse != null)
                        {
                            Patient p = new Patient(tmpPatMain.get(0),patID,tmpPatMain.get(2),tmpPatMain.get(3), patDR,
                                    patNurse,tmpPatMain.get(6).charAt(0), followUp, tmpPatIllnesses, tmpPatMeds, tmpPatVisits);
                            db.getPatients().add(p);
                            //System.out.println("patient  " + p.getName() + " added");
                        }

                    }
                }
            }


            //Dr pt 2
            //convert each element in each doctor's tmpPatID <String> List to an int and add
            // corresponding patient to their patient list
            for (Doctor doctor : db.getDoctors())
            {
                for (String patID: doctor.getPatientIDList())
                {
                    try {
                        Patient p = SearchLogic.findPatient(db, Integer.parseInt(patID));
                        dl.addPatientToList(p, doctor);
                    }
                    catch (PatientNotFoundException PNFE)
                    {
                        System.out.println(PNFE.getMessage());
                    }
                    catch (InvalidInsuranceException IIE)
                    {
                        System.out.println(IIE.getMessage());
                    }
                }
            }
        }
        return true;
    }

    //Main used while testing
    /*
    public static void main(String[] args) {
        Scanner k = new Scanner(System.in);
        String s = k.nextLine();
        File f = new File (s);

        DataBase d = new DataBase();
        LoadDataBase ldb = new LoadDataBase(d);
        ldb.loadData(f);
        System.out.println(d.getNurses().get(0).getName());
        System.out.println(d.getNurses().get(2).getID());

        System.out.println(d.getDoctors().get(0).getName());
        System.out.println(d.getDoctors().get(3).getName());
        for (Patient pat : d.getDoctors().get(3).getPatientList())
        {
            System.out.println(pat.getName());
        }
        System.out.println(d.getPatients().get(1).getVisitationList().get(1).getVisit());
    }*/
}
