/**
 * SaveDataBase.java:
 * This class outputs data to a file to save the current state of the hospital. Users must provide
 * the output file location.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Controllers.DataBaseLogic;

import Models.*;
import Models.People.*;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SaveDataBase
{
    //instance vars

    private DataBase db;

    /**
     * Constructor:
     * takes in a DataBase object and instantiates SaveDataBase to be used in Main.
     *
     * @param db DataBase object
     */
    public SaveDataBase(DataBase db)
    {
        this.db = db;
    }

    /**
     * saveData:
     * This method creates a new file and writes out all the data held within the database. It
     * saves the current hospital state to a file which can be used by loadData at a later point
     * in time.
     */
    public boolean saveData()
    {
        Scanner keyboard = new Scanner(System.in);
        //construct path for file
        System.out.println("Enter start of path for output file (ie: C:\\Users\\... up to last \\ before file name):");
        java.util.Date date = new java.util.Date();
        String fileName = keyboard.nextLine();
        String dateStr = date.toString().replaceAll(":", "-");
        dateStr = dateStr.replaceAll(" ", "_");
        fileName += dateStr;
        fileName += ".txt";

        //create file connection
        File output;
        FileWriter outFile;

        try
        {
            output = new File(fileName);
            outFile = new FileWriter(output);

            //Nurses
            outFile.write(">Nurses");
            for (Nurse nurse : db.getNurses())
            {
                outFile.write("\n"+ nurse.getName() + "," + nurse.getID() + "," + nurse.getPassword());
            }
            outFile.write("\n<");

            //Doctors
            outFile.write("\n>Doctors");
            for (Doctor doctor : db.getDoctors())
            {
                outFile.write("\n~" + doctor.getName() + "," + doctor.getID() + "," + doctor.getPassword());
                outFile.write("\nP*");
                for(int i = 0; i < doctor.getPatientIDList().size(); i++)
                {
                    outFile.write(doctor.getPatientIDList().get(i));
                    if(i!= doctor.getPatientIDList().size()-1 )
                    {
                        outFile.write(",");
                    }
                }
                outFile.write("\nI*");
                for(int i = 0; i < doctor.getAcceptedInsuranceList().size(); i++)
                {
                    outFile.write(doctor.getAcceptedInsuranceList().get(i));
                    if(i!= doctor.getAcceptedInsuranceList().size()-1 )
                    {
                        outFile.write(",");
                    }
                }
                outFile.write("\n");
            }
            outFile.write("<");

            //Patients
            outFile.write("\n>Patients");
            for (Patient patient : db.getPatients())
            {
                outFile.write("\n~" + patient.getName() + "," + patient.getID() + "," + patient.getInsurancePlan()
                        + "," + patient.getDateOfBirth() + "," + patient.getDoctor().getID() + ","
                        + patient.getNurse().getID() + "," + patient.getGender() + "," + patient.getIsFollowedUpWith());
                outFile.write("\nIl*");
                for(int i = 0; i < patient.getIllnessList().size(); i++)
                {
                    outFile.write(patient.getIllnessList().get(i));
                    if(i!= patient.getIllnessList().size()-1 )
                    {
                        outFile.write(",");
                    }
                }
                outFile.write("\nM*");
                for(int i = 0; i < patient.getMedicationList().size(); i++)
                {
                    outFile.write(patient.getMedicationList().get(i));
                    if(i!= patient.getMedicationList().size()-1 )
                    {
                        outFile.write(",");
                    }
                }
                outFile.write("\nV*");
                for (Visitation visit : patient.getVisitationList())
                {
                    outFile.write("@" + visit.getMonth() + "," + visit.getDay() + "," + visit.getYear() + ","
                            + visit.getHours() + "," + visit.getMinutes() + "," + visit.isCheckUp() +"\n");
                }
            }
            outFile.write("\n<");

            outFile.close();
            System.out.println("Done. Database was saved to file @ " + fileName);
            return true;

        } catch (IOException IOE)
        {
            System.err.println("Error. Path not valid. Please Try again!");
            return false;
        }


    }

    //main used for testing
    /*
    public static void main(String[] args) {
        DataBase db = new DataBase();
        LoadDataBase ldb = new LoadDataBase(db);
        Scanner k = new Scanner(System.in);
        String s = k.nextLine();
        File f = new File(s);
        ldb.loadData(f);
        SaveDataBase sb = new SaveDataBase(db);
        sb.saveData();
    }
    */
}
