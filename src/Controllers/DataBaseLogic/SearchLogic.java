/**
 * SearchLogic.java:
 * This class abstracts the logic needed for searching the database for nurses, doctors, and patients
 * by ID number.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */

package Controllers.DataBaseLogic;

import Controllers.Exceptions.DoctorNotFoundException;
import Controllers.Exceptions.NurseNotFoundException;
import Controllers.Exceptions.PatientNotFoundException;
import Models.DataBase;
import Models.People.*;

public class SearchLogic
{
    /**
     * findDoctor:
     * This method takes in a database object and doctor id number and searches the database to return
     * the doctor object that matches the provided id number.
     *
     * @param db DataBase object
     * @param doctorID doctor id int
     * @return Doctor object corresponding to doctor id
     * @throws DoctorNotFoundException Exception for given doctor id not found in the database
     */
    public static Doctor findDoctor (DataBase db, int doctorID) throws DoctorNotFoundException
    {
        Doctor doctor = null;
        for (Doctor d: db.getDoctors())
        {
            if (d.getID() == doctorID)
            {
                doctor = d;
            }
        }
        if (doctor == null)
        {
            throw new DoctorNotFoundException();
        }
        return doctor;
    }

    /**
     * findNurse:
     * This method takes in a database object and nurse id number and searches the database to return
     * the nurse object that matches the provided id number.
     *
     * @param db DataBase object
     * @param nurseID nurse id int
     * @return Nurse object corresponding to nurse id
     * @throws NurseNotFoundException Exception for given nurse id not found in the database
     */
    public static Nurse findNurse (DataBase db, int nurseID) throws NurseNotFoundException
    {
        Nurse nurse = null;
        for (Nurse n: db.getNurses())
        {
            if (n.getID() == nurseID)
            {
                nurse = n;
            }
        }
        if (nurse == null)
        {
            throw new NurseNotFoundException();
        }
        return nurse;
    }

    /**
     * findPatient:
     * This method takes in a database object and patient id number and searches the database to return
     * the patient object that matches the patient id number.
     *
     * @param db DataBase object
     * @param patientID patient id int
     * @return Patient object corresponding to patient id
     * @throws PatientNotFoundException Exception for given patient id not found in the database
     */
    public static Patient findPatient (DataBase db, int patientID) throws PatientNotFoundException
    {
        Patient patient = null;
        for (Patient p: db.getPatients())
        {
            if (p.getID() == patientID)
            {
                patient = p;
            }
        }
        if (patient == null)
        {
            throw new PatientNotFoundException();
        }
        return patient;
    }
}
