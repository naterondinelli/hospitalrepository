/**
 * InvalidInsuranceException.java:
 * This class handles the issue of a patient's insurance not matching with a doctors accepted insurances.
 * It is thrown by addPatientToList method in DoctorLogic.java.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Controllers.Exceptions;

public class InvalidInsuranceException extends Exception
{
    public InvalidInsuranceException()
    {
        super("Patient's insurance is not accepted by this doctor. Patient registration canceled.");
    }
}
