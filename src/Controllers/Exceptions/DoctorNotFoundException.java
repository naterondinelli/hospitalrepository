/**
 * DoctorNotFoundException.java:
 * This class handles the issue of searching for a doctor that is not within the current hospital state.
 * It is thrown by findDoctor method in SearchLogic.java.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Controllers.Exceptions;

public class DoctorNotFoundException extends Exception
{
    public DoctorNotFoundException()
    {
        super("Doctor not found in the database.");
    }
}
