/**
 * PatientNotFoundException.java:
 * This class handles the issue of searching for a patient that is not within the current hospital state.
 * It is thrown by findPatient method in SearchLogic.java.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Controllers.Exceptions;

public class PatientNotFoundException extends Exception
{
    public PatientNotFoundException()
    {
        super("Patient not found in list.");
    }
}
