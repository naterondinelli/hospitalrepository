/**
 * NurseNotFoundException.java:
 * This class handles the issue of searching for a nurse that is not within the current hospital state.
 * It is thrown by findNurse method in SearchLogic.java.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */
package Controllers.Exceptions;

public class NurseNotFoundException extends Exception
{
    public NurseNotFoundException()
    {
        super("Nurse not found in the database.");
    }
}
