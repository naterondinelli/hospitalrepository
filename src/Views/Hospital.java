/**
 * Hospital.java:
 * This class will serve as our main class for the project
 * This runs the main program, pulling models to hold data and controllers to perform the logic.
 *
 * @authors Nate Rondinelli, Devin Schiffer
 */


package Views;

import Controllers.DataBaseLogic.LoadDataBase;
import Controllers.DataBaseLogic.SaveDataBase;
import Controllers.DataBaseLogic.SearchLogic;
import Controllers.Exceptions.DoctorNotFoundException;
import Controllers.Exceptions.InvalidInsuranceException;
import Controllers.Exceptions.NurseNotFoundException;
import Controllers.Exceptions.PatientNotFoundException;
import Controllers.PeopleLogic.DoctorLogic;
import Controllers.PeopleLogic.EmployeeLogic;
import Controllers.PeopleLogic.NurseLogic;
import Models.*;
import Models.People.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Scanner;

public class Hospital
{

    public static void main(String[] args) {
        //Instance Vars & initialize logic controllers
        DataBase dataBase = new DataBase();
        LoadDataBase loadDataBase = new LoadDataBase(dataBase);
        SaveDataBase saveDataBase = new SaveDataBase(dataBase);
        EmployeeLogic employeeLogic = new EmployeeLogic(dataBase);
        DoctorLogic doctorLogic = new DoctorLogic();
        NurseLogic nurseLogic = new NurseLogic();
        Scanner keyboard = new Scanner(System.in);
        String inputFilePath;
        boolean fileLoaded = false, fileSaved = false;
        Doctor currentDoctor = null;
        Nurse currentNurse = null;
        boolean userIsDoctor = true; //true for dr, false for nurse
        int month, day, year;
        month = LocalDateTime.now().getMonthValue();
        day = LocalDateTime.now().getDayOfMonth();
        year = LocalDateTime.now().getYear();


        //Intro to app, set up database by loading from file.
        System.out.println("Welcome to Sweng Hospital Management System.");
        while (!fileLoaded)
        {
            System.out.println("Please enter the (absolute) path to the most recent database save file: ");
            inputFilePath = keyboard.nextLine();
            File inputFile = new File(inputFilePath);
            fileLoaded = loadDataBase.loadData(inputFile);
        }
        System.out.println("Database loaded! Welcome user!\n");
        //log in user
        boolean login = false;
        int loginID = 0;
        String loginPass;
        while(!login)
        {
            System.out.println("Please enter your ID:");
            loginID = keyboard.nextInt();
            System.out.println("Please enter your Password:");
            loginPass = keyboard.next();

            login = employeeLogic.isValidEmployeeLogin(loginID,loginPass);
        }
        if(loginID >= 200)
        {
            try
            {
                currentDoctor = SearchLogic.findDoctor(dataBase,loginID);
                System.out.println("Welcome " + currentDoctor.getName()+"\n");
            }
            catch (DoctorNotFoundException DNFE)
            {
                System.err.println(DNFE.getMessage());
            }
        }
        else if(loginID >= 100)
        {
            try
            {
                currentNurse = SearchLogic.findNurse(dataBase,loginID);
                System.out.println("Welcome " + currentNurse.getName()+"\n");
                userIsDoctor = false;
            }
            catch (NurseNotFoundException NNFE)
            {
                System.err.println(NNFE.getMessage());
            }
        }

        int mainChoice = 0;
        //main loop with options of management
        while(mainChoice != 5)
        {
            System.out.println("What would you like to do today? Select an option (1-5): "+
                    "\n[1] Register new patient" +
                    "\n[2] Schedule appointment" +
                    "\n[3] View / cancel future appointments" +
                    "\n[4] Patient's History and info" +
                    "\n[5] Quit");

            mainChoice = keyboard.nextInt();


            switch (mainChoice)
            {
                case 1:
                //registering new patient
                    if(userIsDoctor)
                    {
                        try
                        {
                            Patient newPatient = employeeLogic.registerPatient(currentDoctor);
                            assert currentDoctor != null;
                            dataBase.getPatients().add(newPatient);
                            doctorLogic.addPatientToList(newPatient, newPatient.getDoctor());
                            newPatient.getDoctor().getPatientIDList().add(""+ newPatient.getID());
                            System.out.println("Patient " + newPatient.getName() + " has been added to the system.\n");

                        } catch (InvalidInsuranceException IIE)
                        {
                            System.err.println(IIE.getMessage());
                        }
                    }
                    else
                    {
                        try
                        {
                            Patient newPatient = employeeLogic.registerPatient(currentNurse);
                            dataBase.getPatients().add(newPatient);
                            doctorLogic.addPatientToList(newPatient, newPatient.getDoctor());
                            newPatient.getDoctor().getPatientIDList().add(""+ newPatient.getID());
                            System.out.println("Patient " + newPatient.getName() + " has been added to the system.\n");
                        } catch (InvalidInsuranceException IIE)
                        {
                            System.err.println(IIE.getMessage());
                        }
                    }

                    break;

                case 2:
                    //scheduling
                    boolean validChoice = false;
                    int tmpPatientID = 0;
                    Patient schPatient = null;
                    System.out.println("Choose a patient ID from below: ");

                    if(userIsDoctor) // for doctor
                    {
                        assert currentDoctor != null;
                        for(Patient patient : currentDoctor.getPatientList())
                        {
                            System.out.println(patient.getName() + " ID: " + patient.getID());
                        }

                        while (!validChoice)
                        {
                            System.out.println("Enter an ID: ");
                            tmpPatientID = keyboard.nextInt();
                            try
                            {
                                schPatient = SearchLogic.findPatient(dataBase, tmpPatientID);
                                if(currentDoctor.getPatientList().contains(schPatient))
                                {
                                    validChoice = true;
                                } else
                                {
                                    throw new PatientNotFoundException();
                                }
                            } catch (PatientNotFoundException PNFE)
                            {
                                System.out.println(PNFE.getMessage());
                            }
                        }
                        doctorLogic.scheduleVisitation(schPatient);
                    }
                    else //for nurse
                    {
                        for(Patient patient : dataBase.getPatients())
                        {
                            System.out.println(patient.getName() + " ID: " + patient.getID());
                        }
                        while (!validChoice)
                        {
                            System.out.println("Enter an ID: ");
                            tmpPatientID = keyboard.nextInt();
                            try
                            {
                                schPatient = SearchLogic.findPatient(dataBase, tmpPatientID);
                                validChoice = true;
                            } catch (PatientNotFoundException PNFE)
                            {
                                System.err.println(PNFE.getMessage());
                            }
                        }
                        nurseLogic.scheduleVisitation(schPatient);
                    }
                    break;

                case 3:
                    for (Patient patient : dataBase.getPatients())
                    {
                        System.out.println("ID: " + patient.getID() +"\tPatient: " + patient.getName());
                        patient.printVisitationList(month,day,year);
                    }

                    System.out.println("Would you like to cancel an appointment?" +
                            "\n[1] Yes" +
                            "\n[2] No");
                    int cancelChoice = keyboard.nextInt();
                    if (cancelChoice == 1)
                    {
                        System.out.println("Enter patient ID: ");
                        int tempPatID = keyboard.nextInt();
                        System.out.println("Enter the corresponding appointment index: ");
                        int apptIndex = keyboard.nextInt();
                        apptIndex--;

                        try
                        {
                            Patient tempPatient = SearchLogic.findPatient(dataBase,tempPatID);
                            if(userIsDoctor)
                            {
                                doctorLogic.cancelVisitation(tempPatient, apptIndex);
                            } else {
                                nurseLogic.cancelVisitation(tempPatient, apptIndex);
                                System.out.println("Appointment cancelled.\n");
                            }

                        } catch (PatientNotFoundException PNFE)
                        {
                            System.err.println(PNFE.getMessage());
                        } catch (IndexOutOfBoundsException IOOBE)
                        {
                            System.err.println("Invalid index.");
                        }
                    }

                    break;

                case 4:
                    //print patient's visitation history and info
                    Patient historyPatient = null;
                    validChoice = false;

                    System.out.println("Choose a patient ID from below: ");
                    for (Patient patient : dataBase.getPatients())
                    {
                        System.out.println("ID: " + patient.getID() +"\tPatient: " + patient.getName());
                    }
                    System.out.println();
                    while(!validChoice)
                    {
                        System.out.println("Enter patient ID:");
                        int historyPatientID = keyboard.nextInt();

                        try
                        {
                            historyPatient = SearchLogic.findPatient(dataBase, historyPatientID);
                            validChoice = true;
                        } catch (PatientNotFoundException PNFE)
                        {
                            System.err.println(PNFE.getMessage());
                        }
                        assert historyPatient != null;
                        employeeLogic.getInfo(historyPatient);
                    }
                    break;

                case 5:
                    //
                    System.out.println("Would you like to save changes?" +
                        "\n[1] Yes" +
                        "\n[2] No");
                    int choice = keyboard.nextInt();
                    if (choice == 1)
                    {
                        while(!fileSaved)
                        {
                            fileSaved = saveDataBase.saveData();
                        }

                    } else if (choice == 2)
                    {
                        System.out.println("Are you sure you want to QUIT WITHOUT SAVING?" +
                                "\n[1] Yes" +
                                "\n[2] No, return to main menu.");
                        choice = keyboard.nextInt();
                        if (choice == 1)
                        {
                            System.out.println("Program ending without saving. Goodbye.");
                            return;
                        }  else
                        {
                            System.out.println("Returning to main menu.");
                            mainChoice = 0;
                        }

                    } else
                    {
                        System.out.println("Invalid input. Returning to main menu.");
                        mainChoice = 0;
                    }
                    break;

                default :
                    //invalid
                    System.out.println("Invalid selection.");
                    break;
            }
        }
    }

}